FROM node:10

ENV PORT=80

WORKDIR /BitfinexInfoBot

COPY package.json yarn.lock /BitfinexInfoBot/
RUN yarn install && yarn cache clean

COPY . /BitfinexInfoBot/

COPY docker-entrypoint.sh  /usr/local/bin/
RUN chmod u+x /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 80

CMD ["node", "/BitfinexInfoBot/serve.js"]
