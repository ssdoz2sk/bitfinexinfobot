module.exports = {
  "development": {
    "username": "CryptoWallet",
    "password": "password",
    "database": "CryptoWallet",
    "host": "127.0.0.1",
    "port": 33302,
    "dialect": "mysql"
  },
  "test": {
    "username": "root",
    "password": null,
    "database": "CryptoWallet",
    "host": "127.0.0.1",
    "port": 3306,
    "dialect": "mysql"
  },
  "production": {
    "username": process.env.DB_USERNAME,
    "password": process.env.DB_PASSWORD,
    "database": process.env.DB_NAME,
    "host": process.env.DB_HOSTNAME,
    "dialect": "mysql",
    "logging": false
  }
}
