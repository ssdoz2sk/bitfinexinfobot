const app = require('./app')

const host = process.env.HOST || 'localhost'
const port = process.env.PORT || 3001
const server = app.listen(port, host)

// 正式啟動伺服器
// 把 app 獨立出來給測試用，而不是正式啟動伺服器
console.info(`Listening to http://${host}:${port} 🚀`)
