const Router = require('koa-router')

const views = require('../views/bot')

const router = new Router({ prefix: '/bot' })

const botToken = process.env.BOT_TOKEN


router
  .post(`/bot${botToken}`, views.bot)
  .post(`/alert${botToken}`, views.alert)
  .post(`/target${botToken}`, views.target)


module.exports = router