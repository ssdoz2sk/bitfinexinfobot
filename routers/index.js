const combineRouters = require('koa-combine-routers')
const botRouters = require('./botRouter')

const router = combineRouters(
  botRouters
)

module.exports = router