const Binance = require('../utils/binance')
const Model = require('../models')

const views = {}

views.bot = async (ctx, next) => {
  ctx.bot.processUpdate(ctx.request.body)
  ctx.status = 200
}

views.alert = async (ctx, next) => {
  let type = ctx.request.body.type
  
  if (type === "margins") {
    ctx.bot.sendMarginInfo(process.env.ADMIN_ID)
    return ctx.status = 200
  } else if (type === "wallet") {
    ctx.bot.sendWalletInfo(process.env.ADMIN_ID)
    return ctx.status = 200
  } else if (type === "chart") {
    ctx.bot.sendWalletChart(process.env.ADMIN_ID)
  }
  return ctx.throw(400, "bad request")
}

views.target = async (ctx, next) => {
  const walletInfo = await Binance.getWallets()
  const dWalletInfo = await Binance.getDWallets()
  const fWalletInfo = await Binance.getFWallets()

  balance = [...walletInfo, ...dWalletInfo].reduce((balance, wallet) => {
    return balance + parseFloat(wallet.amountUSD)
  }, 0)

  balance += fWalletInfo.reduce((balance, wallet) => {
    return balance + parseFloat(wallet.amount)
  }, 0)

  await Model.BinanceWallet.create({balance})
  return ctx.status = 200
}

module.exports = views