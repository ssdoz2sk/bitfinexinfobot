const { RESTv2 } = require('bitfinex-api-node')

const Bitfinex = function () {
  this.rest = new RESTv2({
    apiKey: process.env.BITFINEX_API_KEY,
    apiSecret: process.env.BITFINEX_SECRET
  })
  this.symbolMap = new Map()
  this._initSymbolMap()
}

Bitfinex.prototype._initSymbolMap = async function () {
  const rsymbols = (await this.rest.currencies())[1]
  for (const symbol of rsymbols) {
    // 忽略掉 derivatives (衍生品) 的部份，衍生品的MAP是 ETHF0:USTF0
    if (symbol[0] && symbol[0].endsWith('F0')) {
      continue
    }
    this.symbolMap.set(symbol[1], symbol[0])
  }
}

Bitfinex.prototype.symbolForWallet = function (w) {
  
  const currency = this.symbolMap.get(w.currency) || w.currency
  if (currency.length === 3) {
    return `t${currency}USD`
  }
  return `t${currency}:USD`
}

Bitfinex.prototype.getBalances = async function () {
  // const symbolMap = 
  // 獲取所有的錢包
  const allWallets = await this.rest.balances()
  
  // 去除空錢包
  const balances = allWallets.filter(w => !(+w.amount <= 0.000001)).map(w => ({
    ...w,
    currency: w.currency.toUpperCase()
  }))

  if (balances.length === 0) {
    return []
  }

  const lastPrices = {}
  const symbols = Array.from(new Set(balances.filter(b => b.currency !== 'USD').map(b => this.symbolForWallet(b))))
  
  if (symbols.length > 0) {
    const tickers = await this.rest.tickers(symbols)
    tickers.forEach(element => {
      lastPrices[element[0]] = +element[1]
    })
  }
  let totalValue = 0

  for (const b of balances) {
    const value = +b.amount
   
    if(b.currency === 'USD') {
      totalValue += value
      b.usdValue = value
    } else {
      b.symbolWallet = this.symbolForWallet(b)
      totalValue += value * lastPrices[b.symbolWallet]
      b.usdValue = value * lastPrices[b.symbolWallet]
    }
  }

  return {total: totalValue, wallets: balances}
}

Bitfinex.prototype.getMarginInfo = async function () {
  const positions = await this.rest.positions()
  const lastPrices = {}
  const symbols = positions.map(m => m[0])
  
  if (symbols.length > 0) {
    const tickers = await this.rest.tickers(symbols)
    tickers.forEach(element => {
      lastPrices[element[0]] = +element[1]
    })
  }
  let totalValue = 0
  let margins = []

  for (const p of positions) {
    const ticker = p[0]
    const amount = p[2]
    const basePrice = p[3]
    const lastPrice = lastPrices[ticker]
    const liq = p[8]

    const value = (lastPrice - basePrice) * amount
    totalValue += value
    margins.push({ticker, amount, basePrice, lastPrice, liq, value})
  }

  return {total: totalValue, margins}
}


module.exports = Bitfinex

if (require.main === module) {
  const b = new Bitfinex()
  b.getMarginInfo()
    .then(data => {
      console.log(data)
    })
}

