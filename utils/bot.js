const TelegramBot = require('node-telegram-bot-api')
const Bitfinex = require('./bitfinex')
const Binance = require('./binance')
const { ChartJSNodeCanvas } = require('chartjs-node-canvas');
const moment = require('moment')

const bitfinex = new Bitfinex()
const Sequelize = require('sequelize');
const Model = require('../models');

const bot = function (token, options) {
  const bot = new TelegramBot(token, options)

  // 增加 command 的事件觸發
  bot.on('message', (msg) => {
    let is_text = !! msg.text
    if (is_text && msg.text.startsWith('/')) {
      let t = msg.text.trim().split(/\s+/)
      let command = t[0].split('@')[0].substring(1).toLocaleLowerCase()
      let args = t.slice(1)
      msg.args = args
      bot.emit(`command_${command}`, msg)
    }
  })

  // 下 /status ，目的是檢查群組是否為 supergroup，
  bot.on('command_chat_status', (msg) => {
    bot.sendMessage(msg.chat.id, `chat id: ${msg.chat.id} \ntype:  ${msg.chat.type} \ntitle: ${msg.chat.title}`)
  })

  bot.on('command_bitfinex_wallet', async (msg) => {
    if (String(msg.from.id) !== process.env.ADMIN_ID &&
        msg.from.username !== process.env.ADMIN_USERNAME) {
      return
    }
    bot.sendBitfinexWalletInfo(msg.chat.id)
  })

  bot.on('command_bitfinex_margins', async (msg) => {
    if (String(msg.from.id) !== process.env.ADMIN_ID &&
        msg.from.username !== process.env.ADMIN_USERNAME) {
      return
    }
    bot.sendBitfinexMarginInfo(msg.chat.id)
  })


  bot.on('command_binance_wallet', async (msg) => {
    if (String(msg.from.id) !== process.env.ADMIN_ID &&
        msg.from.username !== process.env.ADMIN_USERNAME) {
      return
    }
    bot.sendBinanceWalletInfo(msg.chat.id)
  })

  bot.on('command_binance_dwallet', async (msg) => {
    if (String(msg.from.id) !== process.env.ADMIN_ID &&
        msg.from.username !== process.env.ADMIN_USERNAME) {
      return
    }
    bot.sendBinanceDWalletInfo(msg.chat.id)
  })

  bot.on('command_binance_fwallet', async (msg) => {
    if (String(msg.from.id) !== process.env.ADMIN_ID &&
        msg.from.username !== process.env.ADMIN_USERNAME) {
      return
    }
    bot.sendBinanceFWalletInfo(msg.chat.id)
  })

  bot.on('command_binance_wallet_chart_5m', async (msg) => {
    if (String(msg.from.id) !== process.env.ADMIN_ID &&
        msg.from.username !== process.env.ADMIN_USERNAME) {
      return
    }
    bot.sendBinanceWalletChart5m(msg.chat.id)
  })

  bot.on('command_binance_wallet_chart_30m', async (msg) => {
    if (String(msg.from.id) !== process.env.ADMIN_ID &&
        msg.from.username !== process.env.ADMIN_USERNAME) {
      return
    }
    bot.sendBinanceWalletChart30m(msg.chat.id)
  })

  bot.on('command_binance_wallet_chart_1d', async (msg) => {
    if (String(msg.from.id) !== process.env.ADMIN_ID &&
        msg.from.username !== process.env.ADMIN_USERNAME) {
      return
    }
    bot.sendBinanceWalletChart1d(msg.chat.id)
  })

  bot.sendBitfinexWalletInfo = async function (chatId) {
    const balances = await bitfinex.getBalances()
    bot.sendMessage(chatId, `Total: ${balances.total.toFixed(3)}`)
    let responseText = ''
    for (let wallet of balances.wallets) {
      responseText += `Type: ${wallet.type} Curr: ${wallet.currency}\n`
      responseText += `Amount: ${wallet.amount} (${wallet.usdValue.toFixed(3)})\n\n`
    }
    bot.sendMessage(chatId, responseText)
  }

  bot.sendBitfinexMarginInfo = async function (chatId) {
    const marginInfo = await bitfinex.getMarginInfo()
    bot.sendMessage(chatId, `Total: ${marginInfo.total.toFixed(3)}`)
    let responseText = ''
    for (let margin of marginInfo.margins) {
      responseText += `Ticker: ${margin.ticker}\n`
      responseText += `Amount: ${margin.amount}\n`
      responseText += `Base: ${margin.basePrice}, Now: ${margin. lastPrice}\n`
      responseText += `Liq: ${margin.liq.toFixed(3)}\n`
      responseText += `Value: ${margin.value.toFixed(3)}\n\n`
    }
    bot.sendMessage(chatId, responseText)
  }

  
  bot.sendBinanceWalletInfo = async function (chatId) {
    const walletInfo = await Binance.getWallets()

    let responseText = ''
    for (let wallet of walletInfo) {
      responseText += `${wallet.asset}: ${wallet.amount} (${wallet.amountUSD})\n`
    }
    if (responseText) {
      bot.sendMessage(chatId, responseText)
    }
  }

  bot.sendBinanceFWalletInfo = async function (chatId) {
    const walletInfo = await Binance.getFWallets()

    let responseText = ''
    for (let wallet of walletInfo) {
      responseText += `${wallet.asset}: ${wallet.amount}\n`
      responseText += `   Balance: ${wallet.balance}\n`
      responseText += `   CrossUnPnl: ${wallet.crossUnPnl}\n`
    }
    if (responseText) {
      bot.sendMessage(chatId, responseText)
    }
  }

  bot.sendBinanceDWalletInfo = async function (chatId) {
    const walletInfo = await Binance.getDWallets()

    let responseText = ''
    for (let wallet of walletInfo) {
      responseText += `${wallet.asset}: ${wallet.amount} (${wallet.amountUSD})\n`
      responseText += `   Balance: ${wallet.balance} (${wallet.balanceUSD})\n`
      responseText += `   CrossUnPnl: ${wallet.crossUnPnl} (${wallet.crossUnPnlUSD})\n`
    }
    if (responseText) {
      bot.sendMessage(chatId, responseText)
    }
  }

  bot.generateImage = async function (balance, labels) {
    const width = 800 //px
    const height = 450 //px
    const chartJSNodeCanvas = new ChartJSNodeCanvas({ width, height})
    
    const configuration = {
      type: 'bar',
      data: {
          labels: labels,
          datasets: [{
              label: '# of Votes',
              data: balance,
              borderColor: "rgba(54, 162, 235, 1)",
              backgroundColor: "rgba(54, 162, 235, 0.2)",
              fill: true,
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              y: {beginAtZero: false}
          }
      }
    }

    return new Promise(async (resolve, reject) => {
      const image = await chartJSNodeCanvas.renderToBuffer(configuration)
      resolve(image)
    })
  }

  bot.sendBinanceWalletChart5m = async function (chatId) {
    const wallets = await Model.BinanceWallet.findAll({
      where: Sequelize.literal(`DATE_FORMAT(\`createdAt\`,'%i')%5 = 0`),
      limit: 288,
      order: [['createdAt', 'DESC']]
    })

    const balance = wallets.map(w => w.balance).reverse()
    const labels = wallets.map(w => moment(w.createdAt).format('HH:mm')).reverse()
    const image = await bot.generateImage(balance, labels)

    const success = await bot.sendPhoto(chatId, image)
  }

  bot.sendBinanceWalletChart30m = async function (chatId) {
    const wallets = await Model.BinanceWallet.findAll({
      where: Sequelize.literal(`DATE_FORMAT(\`createdAt\`,'%i')% 30 = 0`),
      limit: 288,
      order: [['createdAt', 'DESC']]
    })

    const balance = wallets.map(w => w.balance).reverse()
    const labels = wallets.map(w => moment(w.createdAt).format('HH:mm')).reverse()
    const image = await bot.generateImage(balance, labels)

    const success = await bot.sendPhoto(chatId, image)
  }

  bot.sendBinanceWalletChart1d = async function (chatId) {
    const wallets = await Model.BinanceWallet.findAll({
      where: {
        [Sequelize.Op.and]: [
          Sequelize.literal(`DATE_FORMAT(\`createdAt\`,'%k') = 0`),
          Sequelize.literal(`DATE_FORMAT(\`createdAt\`,'%i') = 0`)
        ]
      },
      limit: 60,
      order: [['createdAt', 'DESC']]
    })

    const balance = wallets.map(w => w.balance).reverse()
    const labels = wallets.map(w => moment(w.createdAt).format('HH:mm')).reverse()
    const image = await bot.generateImage(balance, labels)

    const success = await bot.sendPhoto(chatId, image)
  }


  return bot
}

module.exports = bot