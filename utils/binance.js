const Binance = require('node-binance-api')

const binance = new Binance().options({
  APIKEY: process.env.BINANCE_API_KEY,
  APISECRET: process.env.BINANCE_SECRET
})

const getOrders = async function () {
  const orders = await binance.deliveryOpenOrders()

  return orders
}

const getWallets = async function () {
  const ticker = await binance.prices()
  const wallets = await binance.balance()
  const filteredWallet = []

  for (const asset of Object.keys(wallets)) {
    const wallet = wallets[asset]
    const available = parseFloat(wallet.available)
    const onOrder = parseFloat(wallet.onOrder)

    if (available + onOrder <= 0.00001) {
      continue
    }

    if (asset === 'USDT') {
      filteredWallet.push(`${asset}: ${available.toFixed(3)} | ${onOrder.toFixed(3)}`)
    } else {
      const b1 = available * ticker[`${asset}USDT`]
      const b2 = onOrder * ticker[`${asset}USDT`]

      filteredWallet.push({
        asset,
        amount: available + onOrder,
        amountUSD: (b1+b2).toFixed(3),
        available,
        availableUSD: b1.toFixed(3),
        onOrder,
        onOrderUSD: b2.toFixed(3)
      })
    }
  }
  return filteredWallet
}

const getFWallets = async function () {
  const ticker = await binance.prices()
  const fWallets = await binance.futuresBalance()
  const filteredWallet = []

  fWallets.forEach((wallet) => {
    const asset = wallet.asset
    const balance = parseFloat(wallet.balance)
    const crossWalletBalance = parseFloat(wallet.crossWalletBalance)
    const crossUnPnl = parseFloat(wallet.crossUnPnl)
    
    if (Math.abs(balance + crossUnPnl) < 0.00001) {
      return
    }

    filteredWallet.push({
      asset,
      amount: (balance + crossUnPnl).toFixed(3),
      balance: balance.toFixed(3),
      crossUnPnl: crossUnPnl.toFixed(3)
    })
  })
  return filteredWallet
}

const getDWallets = async function () {
  const ticker = await binance.prices()
  const dWallets = await binance.deliveryBalance()
  const filteredWallet = []

  dWallets.forEach((wallet) => {
    const asset = wallet.asset
    const balance = parseFloat(wallet.balance)
    const crossUnPnl = parseFloat(wallet.crossUnPnl)
    
    if (Math.abs(balance + crossUnPnl) < 0.00001) {
      return
    }

    const b1 = balance * ticker[`${asset}USDT`]
    const b2 = crossUnPnl * ticker[`${asset}USDT`]

    filteredWallet.push({
      asset,
      amount: balance + crossUnPnl,
      amountUSD: (b1+b2).toFixed(3),
      balance: balance,
      balanceUSD: b1.toFixed(3),
      crossUnPnl,
      crossUnPnlUSD: b2.toFixed(3)
    })
  })
  return filteredWallet
}

module.exports = {
  getOrders,
  getFWallets,
  getDWallets,
  getWallets
}

if (require.main === module) {
  getOrders()
  getDWallets()
  getWallets()
}