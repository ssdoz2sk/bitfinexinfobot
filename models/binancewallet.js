'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class BinanceWallet extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  BinanceWallet.init({
    balance: DataTypes.DOUBLE
  }, {
    sequelize,
    modelName: 'BinanceWallet',
  });
  return BinanceWallet;
};